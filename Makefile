PREIFX = ~/.local

clean:
	@echo "nothing to do"

install:
	mkdir -p $(PREIFX)/bin/ $(PREIFX)/share/mdocker/
	install -m 700 mdocker $(PREIFX)/bin/
	cp -r templates/ $(PREIFX)/share/mdocker/

uninstall:
	rm -rf $(PREIFX)/bin/mdocker $(PREIFX)/share/mdocker
