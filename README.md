# mdocker

A docker wrapper for setting up and running development environments easier written in POSIX shell.

## Installation

```bash
git clone https://gitlab.com/mhdy/mdocker.git
cd mdocker
make install
```

## Usage

```
Usage:
  mdocker command [ARGS]

Commands:
  init TEMPLATE IMAGE_NAME
              Initialize the current directory with template TEMPLATE
              and specify the docker image name IMAGE_NAME.
  templates   List available templates.
  build       Build a new image and remove the old one.
  run [CMD]   Run the container.
              Generally used to mount the current directory and spawn a tty.
  exec CMD    Execute the command CMD inside a running container.
              CMD is passed as an argument to .docker/exec.sh.
  attach      Attach a tty to the running container.
  lsd         Run a language server daemon for the current project.
              This can be used to link a language client of a code editor.
  clean       Delete the docker image without removing .dockerignore and .docker.
  purge       Delete the docker image, .docker and .dockerignore.
  push        Push the repository image to docker hub.
  notice      Read the notice attached to the current project
  help        Display this help and exit.
```

### Workflow

The mdocker workflow is simple:

1. Create an new project directory and cd into it.
2. Initialize the directory with a template and a docker image name by invoking `mdocker init TEMPLATE IMAGE_NAME`.
   A set of templates are available in the mdocker `templates` directory.
3. Run `mdocker build` to build the docker image using .docker/Dockerfile.
   At this point, you project environment is set up.
4. Run `mdocker run` to access the environment.
5. You can run a command inside the container and exit using `mdocker exec CMD`.

If you want more control on how to build/run/attach/exec, you can edit the corresponding `.docker/*.sh` file. Two variable are made available for these scripts: $WORKING_DIR containing the path of the current mdocker project and $DOCKER_IMAGE containing the docker image name of the current mdocker project.

### Adding templates

A template tree contains a `.docker` directory as well as all files that will be copied by `mdocker init` into the project directory. 
The `.docker/` directory is expected to have 4 files:
- The Dockerfile
- The `build.sh` script that is executed when invoking `mdocker build`. 
  This script contains all the directives to build the environment docker image.
- The `run.sh` script that is executed when invoking `mdocker run`. 
  This script specifies the docker run options such as volume mounts, port forwards, and is expected to spawn a tty.
- The `exec.sh` script that is executed when invoking `mdocker exec`. 
  This script specifies the docker run options such as volume mounts, port forwards, and is expected to run the command given as an argument.
- The `attach.sh` script which is executed when invoking `mdocker attach`.
  This script should specify how to spawn a tty.
- The `lsd.sh` script which is executed when invoking `mdocker lsd`.
  This script should specify how to run a language server. This could be integrated in text editors like vim.

All user created templates should be placed inside `~/.local/share/mdocker/templates` directory.

## License

MIT.
