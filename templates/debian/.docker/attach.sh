#!/bin/sh

container_id="$(docker container ps -q -f ancestor="$DOCKER_IMAGE" | head -1)"

docker exec -i -t "$container_id" /bin/bash
