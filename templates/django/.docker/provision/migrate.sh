#!/bin/sh

project_name=''

rm -rf /app/$project_name/db.sqlite3
ls /app/$project_name/*/migrations/*.py | \
  while read line
  do
    [ "$(basename "$line")" != '__init__.py' ] && rm -f "$line"
  done

python /app/$project_name/manage.py makemigrations
python /app/$project_name/manage.py migrate

su=$(cat <<EOF
from django.contrib.auth import get_user_model
User = get_user_model()
User.objects.create_superuser('root', '', 'toor')
EOF
)
echo "$su" | python /app/$project_name/manage.py shell
