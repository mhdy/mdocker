#!/bin/sh

docker run --rm -i -t \
  -v "$WORKING_DIR":/workspace \
  "$DOCKER_IMAGE" /bin/bash
