#!/bin/sh

dockerfile_path="$WORKING_DIR/.docker/Dockerfile"

docker build -t "$DOCKER_IMAGE" -f "$dockerfile_path" "$WORKING_DIR"
