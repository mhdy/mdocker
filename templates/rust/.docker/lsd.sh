#!/bin/sh

docker run --rm -i \
  -v "$WORKING_DIR":/workspace \
  "$DOCKER_IMAGE" rust-analyzer
